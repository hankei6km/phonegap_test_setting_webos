# coding: utf-8
'''
Created on 2011/10/18

@author: hankei6km
'''

import os
import unittest
import phonegap_test_setting_webos
import shutil
import filecmp

class PhoneGapTestAndroidTestCase(unittest.TestCase):
    def test_get_proj_path(self):
        self.assertEqual(phonegap_test_setting_webos.get_project_path("PhoneGapTestCmd"), os.path.abspath("PhoneGapTestCmd"), "project path not match")

    def test_get_android_nanifest_xml_path(self):
        self.assertEqual(phonegap_test_setting_webos.get_appinfo_json_path(os.path.abspath("PhoneGapTestCmd")), os.path.abspath("PhoneGapTestCmd/appinfo.json"), "appinfo.json path not match")

    def test_chk_argv(self):
        self.assert_(phonegap_test_setting_webos.chk_argv(["", "PhoneGapTestCmd", "assets/www/index.html"]), "normal argv fail(on)")
        self.assert_(phonegap_test_setting_webos.chk_argv(["", "PhoneGapTestCmd", "assets/www/tests/index.html"]), "normal argv fail(off)")
        self.assert_(phonegap_test_setting_webos.chk_argv(["", "PhoneGapTestCmd/", "assets/www/index.html"]), "normal argv fail(path with /)")
        self.assert_(phonegap_test_setting_webos.chk_argv(["", "../tests/PhoneGapTestCmd/", "assets/www/index.html"]), "normal argv fail(parent path)")
        self.assert_(phonegap_test_setting_webos.chk_argv(["", "PhoneGapTestCmd\\", "assets/www/index.html"]), "normal argv fail(path with \\)")
        self.assertFalse(phonegap_test_setting_webos.chk_argv(["", "", ""]), "argv[1] blank")
        self.assertFalse(phonegap_test_setting_webos.chk_argv(["", "test/PhoneGapTestCmd", ""]), "argv[2] blank")
        self.assertFalse(phonegap_test_setting_webos.chk_argv(["", "test/PhoneGapTestCmd1", ""]), "argv[1] invalid")

    def test_replace_load_url(self):
        self.assertEqual(phonegap_test_setting_webos.replace_load_url('    "main": "index.html",', "tests/index.html"), '    "main": "tests/index.html",', "replace tests/index.html 1")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url('    "main":"index.html",', "tests/index.html"), '    "main":"tests/index.html",', "replace tests/index.html 2")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url('    "main" :"index.html",', "tests/index.html"), '    "main" :"tests/index.html",', "replace tests/index.html 3")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url('    "main" : "index.html",', "tests/index.html"), '    "main" : "tests/index.html",', "replace tests/index.html 4")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url("    'main' : 'index.html',", "tests/index.html"), "    'main' : 'tests/index.html',", "replace tests/index.html 5")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url("", "tests/index.html"), "", "blank")
        self.assertEqual(phonegap_test_setting_webos.replace_load_url('    "title": "PhoneGapTestCmd"', "tests/index.html"), '    "title": "PhoneGapTestCmd"', "not match")

    def test_chk_appinfo_json(self):
        appinfo_json=phonegap_test_setting_webos.get_appinfo_json_path(os.path.abspath("PhoneGapTestCmd"))
        self.assertEqual(phonegap_test_setting_webos.chk_appinfo_json(appinfo_json),"","normal appinfo.json")

        appinfo_json=phonegap_test_setting_webos.get_appinfo_json_path(os.path.abspath("PhoneGapTestCmd_invalid_appinfo_json/json_invalid"))
        self.assertRegexpMatches(phonegap_test_setting_webos.chk_appinfo_json(appinfo_json),r"^chk_appinfo_json exception:","json_invalid")

        appinfo_json=phonegap_test_setting_webos.get_appinfo_json_path(os.path.abspath("PhoneGapTestCmd_invalid_appinfo_json/main_not_exit"))
        self.assertEqual(phonegap_test_setting_webos.chk_appinfo_json(appinfo_json),"main item is not exist","main_not_exit")

    def test_reaplce_appinfo_json(self):
        #元になるファイルからテスト用の作業ファイルにコピー
        shutil.copyfile(os.path.abspath("PhoneGapTestCmd_replace_appinfo_json/appinfo_org.json"), os.path.abspath("PhoneGapTestCmd_replace_appinfo_json/appinfo.json"))
        
        work_file = os.path.abspath("PhoneGapTestCmd_replace_appinfo_json/appinfo.json")
        test_on_file=os.path.abspath("PhoneGapTestCmd_replace_appinfo_json/appinfo_test_on.json")
        test_off_file=os.path.abspath("PhoneGapTestCmd_replace_appinfo_json/appinfo_test_off.json")

        phonegap_test_setting_webos.reaplce_appinfo_json(work_file, "tests/index.html")
        self.assert_(filecmp.cmp(test_on_file, work_file, False), "test_on first")

        phonegap_test_setting_webos.reaplce_appinfo_json(work_file, "index.html")
        self.assert_(filecmp.cmp(test_off_file, work_file, False), "test_off first")

        phonegap_test_setting_webos.reaplce_appinfo_json(work_file, "tests/index.html")
        self.assert_(filecmp.cmp(test_on_file, work_file, False), "test_on second")

        phonegap_test_setting_webos.reaplce_appinfo_json(work_file, "index.html")
        self.assert_(filecmp.cmp(test_off_file, work_file, False), "test_off second")

        #作業ファイル削除
        os.remove(work_file)