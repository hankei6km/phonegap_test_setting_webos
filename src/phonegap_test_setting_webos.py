#!/usr/bin/env python
# coding: utf-8
# Copyright (c) 2011 hankei6km
# License: MIT License (http://opensource.org/licenses/mit-license.php)
'''
Created on 2011/10/18
@author: hankei6km


指定されたPhoneGap+webOSプロジェクト（フォルダ）のappinfo.json内に定義されている、mainプロパティを置き換える

これにより、作業中のプロジェクトを実行したときにロードされるページを、通常時のページとテスト用のページとで切り替える

Usage: phonegap_test_setting_webos.py project_folder replace_with_load_url
'''
import sys
import os
import json
import re

def get_project_path(proj_rel_path):
    """ プロジェクトの絶対パスを取得する
    """
    return os.path.abspath(proj_rel_path)

def get_appinfo_json_path(proj_path):
    """ proj_path(絶対パス)から appinfo.json のパスを取得する
    """
    return os.path.join(proj_path, "appinfo.json")


def chk_argv(argv):
    """ 引数のチェック（数とファイルの存在チェック）。妥当なら True、だめならFalse 
    """
    ret = True
    
    try:
        if len(argv) == 3 :
            # AndroidManifest.xmlが存在するか？
            if os.path.exists(get_appinfo_json_path(get_project_path(argv[1]))) == False:
                ret = False
            #on|offチェック
            if(argv[2] == ""):
                ret = False;
        else:
            ret = False
    except TypeError:
        pass
    finally:
        pass
        
    return ret

def chk_appinfo_json(appinfo_json):
    """appinfo.jsonがパースできるか、main項目を含んでいるかをチェックする
    OKなら""を返す、NGならエラー内容を返す
    """
    ret = ""
    
    try:
        with open(appinfo_json, "r") as infile:
            appinfo_obj = json.load(infile)
        
        if ("main" in appinfo_obj) != True:
            ret = "main item is not exist"
            
    except:
        ret = "chk_appinfo_json exception:" + str(sys.exc_info()[:2])
    finally:
        pass
    
    return ret

def replace_load_url(rec_str, replace_with_load_url):
    """レコードにsuper.loadUrl(???);があれば、replace_with_load_urlに置き換える
    """
    r = re.compile(u"([\"']main[\"'].*?:.*?[\"']).*?([\"'])")
    return r.sub("\\1" + replace_with_load_url + "\\2", rec_str);


def reaplce_appinfo_json(appinfo_json, replace_index_html):
    """appinfo.jsonのmain項目を置き換える。
    ファイル内の行の並びなどはできるだけオリジナルを保持しておきたいので、json形式での操作は行わない。
    """
    replaced_rec = []
    with open(appinfo_json, "r") as in_file:
        for rec in in_file:
            replaced_rec.append(replace_load_url(rec, replace_index_html))
            
    with open(appinfo_json, "wb") as out_file:
        for rec in replaced_rec:
            out_file.write(rec)


if __name__ == '__main__':

    if chk_argv(sys.argv):
        appinfo_json_path = get_appinfo_json_path(sys.argv[1])
        err = chk_appinfo_json(appinfo_json_path)
        if err == "":
            reaplce_appinfo_json(appinfo_json_path, sys.argv[2])
        else:
            sys.stderr.write(err)
    else:
        print u"Usage: phonegap_test_setting_webos.py project_folder replace_with_load_url"
        print u"   ex: phonegap_test_setting_webos.py PhoneGapProj tests/index.html"
