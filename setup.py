#!/usr/bin/env python

from setuptools import setup
setup(
    name="phonegap_test_setting_webos",
    version="0.1.0",
    author='hankei6km',
    license='MIT License',
    description='Turn on/off project test setting(replace loadUrl) for PhoneGap webOS',
    url='http://hankei6km.bitbucket.org/',
    scripts=['src/phonegap_test_setting_webos.py']
)
